<h1>Nest Web</h1>
<h2>基于Nest的快速开发框架，Nest真香嗷！</h2>

## 快速启动

### Docker容器外部署

- 创建 .env 文件，然后执行`cp .env.example .env` 命令。替换.env文件中的环境配置（比如mysql/mariadb的链接参数）
- 安装依赖 `yarn`
- 启动服务：`yarn start` （3000端口）

### Docker容器内部署
只需运行已经准备好的bash脚本：:
```bash
$ ./init
```
它会构建Docker镜像，在dev下运行Nest的应用，监听的是80端口


## Test

```bash
# 单元测试
$ docker exec -it nest yarn test

# e2e 测试
$ docker exec -it nest yarn test:e2e

# 测试覆盖率
$ docker exec -it nest yarn test:cov
```

## 配置环境
在configservice中读取 .env中的配置变量。
包括Mysql链接配置等


## Swagger
运行在：http://localhost:3000/api/docs

## TypeORM 集成

[TypeORM](http://typeorm.io/) 
TypeORM作为持久层，支持多数据库、多链接等。更多详情可以参考官方文档。



## JWT鉴权

已经实现了安全的登陆和注册方式
密码散列也已经实现

## 统一的数据返回

support/code中 实现了ApiResult
固定返回格式：
```bash
{
  "timestamp": "2019-10-08T01:52:37.965Z",
  "Data": {},
  "status": 200,
  "message": "请求成功"
}
```

## 统一业务异常处理

support/code中 实现了 throwBusinessException
固定返回格式：
```bash
{
  "timestamp": "2019-10-30T04:24:53.418Z",
  "status": -10005,
  "message": "数据不存在，操作失败"
}
```
## TODO-Redis实现字典和用户权限的缓存
