import { Entity, PrimaryColumn, Column } from 'typeorm';
import { BaseIdEntity } from '../config/index';

@Entity({ name: 'cat' })
export class Cats extends BaseIdEntity {
  @PrimaryColumn()
  id: string;
  @Column({ length: 500 })
  name: string;
  @Column('int')
  age: number;
}
