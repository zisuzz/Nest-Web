import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
export class CreateCats {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: string;
  @ApiModelProperty()
  @IsNotEmpty()
  readonly age: number = 0;
}
