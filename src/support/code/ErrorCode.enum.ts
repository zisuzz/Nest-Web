export enum ErrorCode {
  TIMEOUT = -1, // 系统繁忙
  SUCCESS = 200, // 成功

  USER_ID_INVALID = -10001, // 用户id无效
}
