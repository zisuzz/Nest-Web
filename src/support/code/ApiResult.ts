export class ApiResult {
  private timestamp: any = new Date();
  private status: number;
  private message: string;
  private Data: any;

  constructor(status: number, message: string = '请求成功', data?: any) {
    this.Data = data;
    this.status = status;
    this.message = message;
  }
  /**
   * 带消息的成功响应
   * @param message 消息体
   * @param data 数据
   */
  public static SUCCESS(data: any, message?: string) {
    return new ApiResult(200, message, data);
  }
  /**
   * 带消息的错误响应
   * @param message 消息体
   * @param data 数据
   */
  public static ERROR(status: number, message: string) {
    return new ApiResult(status, message);
  }

  /**
   *
   * @param status 状态码
   * @param message 消息
   * @param data 数据
   */
  public static Error_Filter(status: number , message: string = '请求成功', data?: any) {
    return new ApiResult(status, message, data);
  }
}
